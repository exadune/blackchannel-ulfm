This library implements the functions
- `MPI_Comm_revoke`
- `MPI_Comm_shrink`
- `MPI_Comm_agree`
- `MPI_Comm_iagree`

of the [ULFM proposal](http://fault-tolerance.org) builed up on the plain MPI-3 standart using the P-Interface.

Internally `MPI_Comm_revoke` sends a message to all remote ranks. When receiving this message the communicator is marked to be revoked. When calling MPI_[Wait|Test][_any|_all|_some]? it is checked whether the communicator is revoked. Blocking communications are mapped to nonblocking internally.

**TODO**
- Add more tests
- Implement workarounds for not supported functions

**RESTRICTIONS**
- Functions that create a new communicator are not thread safe
- MPI_[Wait|Test][_any|_all|_some] are not thread safe
- Dispite stated in the standard blocking and nonblocking collectives are matching
- When a collective communication is interrupted by a call of MPIX_Comm_revoke, the communicator can not be freed any more (memory leak).
- Communicator duplication/splitting etc. is not protected. (Does not return when the communicator is revoked)

**NOT SUPPORTED YET**
- I/O
- One-sided-communication
- MPI_Sendrecv[_replace]
- MPI_Comm_idup
- MPI_Imrecv and MPI_Mrecv


**USAGE**

The shared library `libblackchannel-ulfm.so` must be preloaded:
- OpenMPI  
`mpirun -x LD_PRELOAD=libblackchannel-ulfm.so ./app`
- MPICH / IntelMPI (not tested)  
`mpirun -env LD_PRELOAD libblackchannel-ulfm.so ./app`