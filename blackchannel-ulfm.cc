#include <string.h>

#include <unordered_set>
#include <unordered_map>
#include <tuple>
#include <vector>

#include <boost/preprocessor.hpp>
#include <mpi.h>

#include "blackchannel-ulfm.h"

// define MACROS for wrapping functions using BOOST
// see https://stackoverflow.com/questions/44758329/c-or-macro-magic-to-generate-method-and-forward-arguments

// the communicator parameter must be called
// comm and the resulting request req
#define WRAP_NONBLOCKING(fn, ...)                                       \
  int fn(MAGICAL_GENERATE_PARAMETERS(BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))){ \
    checkBlackchannel();                                                \
    Comm_data* data = comm_comm_data[comm];                             \
    if(data->revoked){                                                  \
      *req = MPI_REQUEST_NULL;                                          \
      PMPI_Comm_call_errhandler(comm, MPIX_ERR_REVOKED);                 \
      return MPIX_ERR_REVOKED;                                          \
    }                                                                   \
    int result = P##fn(MAGICAL_GENERATE_ARGUMENTS(BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))); \
    if(result == MPI_SUCCESS && *req != MPI_REQUEST_NULL){              \
      Comm_data* data = comm_comm_data[comm];                           \
      data->requests.insert(*req);                                      \
      request_comm_data[*req] = data;                                   \
    }                                                                   \
    return result;                                                      \
  }

// must be called after the nonblocking corresponding function
// the nonblocking function must take the same arguments as
// the blocking plus a MPI_Request* as last argument
#define WRAP_BLOCKING(fn, nonblocking_fn, ...)                          \
  int fn(MAGICAL_GENERATE_PARAMETERS(BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))){ \
    MPI_Request req;                                                    \
    int result = nonblocking_fn(MAGICAL_GENERATE_ARGUMENTS(BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)), &req); \
    if(result != MPI_SUCCESS)                                            \
      return result;                                                    \
    return MPI_Wait(&req, MPI_STATUS_IGNORE);                           \
  }

// must be called after the nonblocking corresponding function
// the nonblocking function must take the same arguments as
// the blocking plus a MPI_Request* as last argument
#define WRAP_BLOCKING_STATUS(fn, nonblocking_fn, ...)                   \
  int fn(MAGICAL_GENERATE_PARAMETERS(BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))){ \
    MPI_Request req;                                                    \
    int result = nonblocking_fn(MAGICAL_GENERATE_ARGUMENTS(BOOST_PP_SEQ_POP_BACK(BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))), &req); \
    if(result != MPI_SUCCESS)                                           \
      return result;                                                    \
    return MPI_Wait(&req, status);                                      \
  }

// the new communicator parameter must be called newcomm
// the id of the new communicator is the maximum of comm_counter over all ranks
// s.t. it is unique
#define WRAP_NEWCOMM(fn, ...)                                           \
  int fn(MAGICAL_GENERATE_PARAMETERS(BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))){ \
    int result = P##fn(MAGICAL_GENERATE_ARGUMENTS(BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))); \
    if(result == MPI_SUCCESS && *newcomm != MPI_COMM_NULL){              \
      set_comm_counter(*newcomm);                                       \
    }                                                                   \
    return result;                                                      \
  }

#define MAGICAL_GENERATE_PARAMETERS(Args) \
  BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(MAGICAL_MAKE_PARAMETER, %%, Args))

#define MAGICAL_GENERATE_ARGUMENTS(Args) \
  BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(MAGICAL_MAKE_ARGUMENT, %%, Args))

#define MAGICAL_MAKE_PARAMETER(s, Unused, Arg) \
  BOOST_PP_TUPLE_ELEM(2, 0, Arg) BOOST_PP_TUPLE_ELEM(2, 1, Arg)

#define MAGICAL_MAKE_ARGUMENT(s, Unused, Arg) \
  BOOST_PP_TUPLE_ELEM(2, 1, Arg)

// structs
struct Comm_data{
  int id;
  MPI_Comm comm;
  std::unordered_set<MPI_Request> requests = {};
  bool revoked = false;
};

// static variables
std::unordered_map<int, Comm_data> comm_data; // Maps communicator to an int id (s.t. it can be communicated)
std::unordered_map<MPI_Request, Comm_data*> request_comm_data; // Maps requests to their communicator ids
std::unordered_map<MPI_Comm, Comm_data*> comm_comm_data; // Maps requests to their communicator ids
MPI_Comm blackchannel_comm = MPI_COMM_NULL;
MPI_Request blackchannel = MPI_REQUEST_NULL;
int blackchannel_data = -1;
int comm_counter = 0;
constexpr int TAG = 42;

// helper functions
int set_comm_counter(MPI_Comm c){
  int result = PMPI_Allreduce(MPI_IN_PLACE, &comm_counter, 1, MPI_INT, MPI_MAX, c);
  comm_data[comm_counter].id = comm_counter;
  comm_data[comm_counter].comm = c;
  comm_comm_data[c] = &comm_data[comm_counter];
  comm_counter++;
  return result;
}

int create_blackchannel(){
  if(blackchannel_comm == MPI_COMM_NULL){
    int result = PMPI_Comm_dup(MPI_COMM_WORLD, &blackchannel_comm);
    if(result != MPI_SUCCESS)
      return result;
    PMPI_Comm_set_errhandler(blackchannel_comm, MPI_ERRORS_RETURN);
    set_comm_counter(MPI_COMM_SELF);
    set_comm_counter(MPI_COMM_WORLD);
    // dummy for iagree
    comm_data[-1].id = -1;
  }
  if(blackchannel == MPI_REQUEST_NULL){
    blackchannel_data = -1;
    int result = PMPI_Irecv(&blackchannel_data, 1, MPI_INT, MPI_ANY_SOURCE, TAG, blackchannel_comm, &blackchannel);
    if(result != MPI_SUCCESS)
      return result;
  }
  return MPI_SUCCESS;
}

void cleanup(){
  int flag = 0;
  PMPI_Finalized(&flag);
  if(flag)
    return;
  if(blackchannel != MPI_REQUEST_NULL){
    int result = PMPI_Cancel(&blackchannel);
    if(result == MPI_SUCCESS)
      PMPI_Request_free(&blackchannel);
    comm_comm_data.clear();
  }
  if(blackchannel_comm != MPI_COMM_NULL){
    PMPI_Comm_free(&blackchannel_comm);
  }
}

// initialization happens when the library is loaded or when MPI_Init is called
namespace {
  struct initializer {
    initializer() {
      int flag = 0;
      MPI_Initialized(&flag);
      if(flag){
        create_blackchannel(); // either here or in MPI_Init
      }
    }

    ~initializer(){
      cleanup();
    }
  };
  initializer i;
}

Comm_data* get_Comm_data(MPI_Comm& c){
  return comm_comm_data[c];
}

int checkBlackchannel(){
  int flag = 0;
  do{
    int result = PMPI_Test(&blackchannel, &flag, MPI_STATUS_IGNORE);
    if(result != MPI_SUCCESS)
      return result;
    if(flag){
      comm_data[blackchannel_data].revoked = true;
      create_blackchannel();
    }
  }while(flag);
  return MPI_SUCCESS;
}

extern "C" {
  // wrappers
  // Initialization:
  int MPI_Init(int *argc, char ***argv){
    int result = PMPI_Init(argc, argv);
    if(result == MPI_SUCCESS)
      create_blackchannel();
    return result;
  }

  int MPI_Finalize(){
    cleanup();
    return PMPI_Finalize();
  }

  int MPI_Init_thread(int *argc, char ***argv, int required, int *provided){
    int result = PMPI_Init_thread(argc, argv, required, provided);
    if(result == MPI_SUCCESS)
      create_blackchannel();
    return result;
  }

  // Point2Point
  WRAP_NONBLOCKING(MPI_Isend, (const void*, buf),( int, count),( MPI_Datatype, datatype),( int, dest),
            (int, tag),( MPI_Comm, comm),( MPI_Request*, req))
  WRAP_BLOCKING(MPI_Send, MPI_Isend, (const void*, buf), (int, count), (MPI_Datatype, datatype), (int, dest),
           (int, tag), (MPI_Comm, comm))
  WRAP_NONBLOCKING(MPI_Ibsend, (const void*, buf), (int, count), (MPI_Datatype, datatype), (int, dest),
                 (int, tag), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Bsend, MPI_Ibsend, (const void*, buf), (int, count), (MPI_Datatype, datatype), (int, dest),
                (int, tag), (MPI_Comm, comm))
  WRAP_NONBLOCKING(MPI_Issend,(const void*, buf), (int, count), (MPI_Datatype, datatype), (int, dest),
                 (int, tag), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Ssend, MPI_Issend, (const void*, buf), (int, count), (MPI_Datatype, datatype), (int, dest),
                (int, tag), (MPI_Comm, comm))
  WRAP_NONBLOCKING(MPI_Irsend, (const void*, buf), (int, count), (MPI_Datatype, datatype), (int, dest),
                 (int, tag), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Rsend, MPI_Irsend, (const void*, buf), (int, count), (MPI_Datatype, datatype), (int, dest),
                (int, tag), (MPI_Comm, comm))
  WRAP_NONBLOCKING(MPI_Irecv, (void*, buf), (int, count), (MPI_Datatype, datatype), (int, source),
                (int, tag), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING_STATUS(MPI_Recv, MPI_Irecv, (void*, buf), (int, count), (MPI_Datatype, datatype), (int, source),
               (int, tag), (MPI_Comm, comm), (MPI_Status*, status))
  // probe
  int MPI_Iprobe(int source, int tag, MPI_Comm comm, int* flag,
                 MPI_Status* status){
    checkBlackchannel();
    Comm_data* data = get_Comm_data(comm);
    if(data->revoked){
      PMPI_Comm_call_errhandler(comm, MPIX_ERR_REVOKED);
      return MPIX_ERR_REVOKED;
    }
    return PMPI_Iprobe(source, tag, comm, flag, status);
  }

  int MPI_Probe(int source, int tag, MPI_Comm comm, MPI_Status* status){
    int result, flag;
    do{
      result = MPI_Iprobe(source, tag, comm, &flag, status);
    }while(flag == 0 && result == MPI_SUCCESS);
    return result;
  }

  int MPI_Improbe(int source, int tag, MPI_Comm comm, int* flag,
                  MPI_Message* message, MPI_Status* status){
    checkBlackchannel();
    Comm_data* data = get_Comm_data(comm);
    if(data->revoked){
      PMPI_Comm_call_errhandler(comm, MPIX_ERR_REVOKED);
      return MPIX_ERR_REVOKED;
    }
    return PMPI_Improbe(source, tag, comm, flag, message, status);
  }

  int MPI_Mprobe(int source, int tag, MPI_Comm comm, MPI_Message* message,
                 MPI_Status* status){
    int result, flag;
    do{
      result = MPI_Improbe(source, tag, comm, &flag, message, status);
    }while(flag == 0 && result == MPI_SUCCESS);
    return result;
  }

  // WRAP_NONBLOCKING(MPI_Imrecv, (void*, buf), (int, count), (MPI_Datatype, datatype),
  //                  (MPI_Message*, message), (MPI_Request*, req))
  // WRAP_BLOCKING(MPI_Mrecv, MPI_Imrecv, (void*, buf), (int, count), (MPI_Datatype, datatype),
  //               (MPI_Message*, message), (MPI_Status*, status))
  // persistent
  WRAP_NONBLOCKING(MPI_Send_init, (const void*, buf), (int, count), (MPI_Datatype, datatype),
                    (int, dest), (int, tag), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_NONBLOCKING(MPI_Bsend_init, (const void*, buf), (int, count), (MPI_Datatype, datatype),
                     (int, dest), (int, tag), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_NONBLOCKING(MPI_Ssend_init, (const void*, buf), (int, count), (MPI_Datatype, datatype),
                     (int, dest), (int, tag), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_NONBLOCKING(MPI_Rsend_init, (const void*, buf), (int, count), (MPI_Datatype, datatype),
                     (int, dest), (int, tag), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_NONBLOCKING(MPI_Recv_init, (void*, buf), (int, count), (MPI_Datatype, datatype), (int, source),
                    (int, tag), (MPI_Comm, comm), (MPI_Request*, req))
  //send-receive
  // int MPI_Sendrecv(const void* sendbuf, int sendcount, MPI_Datatype sendtype,
  //                  int dest, int sendtag, void* recvbuf, int recvcount,
  //                  MPI_Datatype recvtype, int source, int recvtag, MPI_Comm comm,
  //                  MPI_Status* status){
  //   // no corresponding nonblocking function?!
  // }
  // int MPI_Sendrecv_replace(void* buf, int count, MPI_Datatype datatype,
  //                          int dest, int sendtag, int source, int recvtag, MPI_Comm comm,
  //                          MPI_Status* status){
  //   // no corresponding nonblocking function?!
  // }

  // Collective
  WRAP_NONBLOCKING(MPI_Ibarrier, (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Barrier, MPI_Ibarrier, (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Ibcast, (void*, buffer), (int, count), (MPI_Datatype, datatype), (int, root),
                 (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Bcast, MPI_Ibcast, (void*, buffer), (int, count), (MPI_Datatype, datatype), (int, root),
                (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Igather, (const void*, sendbuf), (int, sendcount), (MPI_Datatype, sendtype),
                  (void*, recvbuf), (int, recvcount), (MPI_Datatype, recvtype), (int, root),
                  (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Gather, MPI_Igather, (const void*, sendbuf), (int, sendcount), (MPI_Datatype, sendtype),
                 (void*, recvbuf), (int, recvcount), (MPI_Datatype, recvtype), (int, root),
                 (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Igatherv, (const void*, sendbuf), (int, sendcount), (MPI_Datatype, sendtype),
                   (void*, recvbuf), (const int*, recvcounts), (const int*, displs),
                   (MPI_Datatype, recvtype), (int, root), (MPI_Comm, comm),
                   (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Gatherv, MPI_Igatherv, (const void*, sendbuf), (int, sendcount), (MPI_Datatype, sendtype),
                  (void*, recvbuf), (const int*, recvcounts), (const int*, displs),
                  (MPI_Datatype, recvtype), (int, root), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Iscatter, (const void*, sendbuf), (int, sendcount), (MPI_Datatype, sendtype),
                   (void*, recvbuf), (int, recvcount), (MPI_Datatype, recvtype), (int, root),
                   (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Scatter, MPI_Iscatter, (const void*, sendbuf), (int, sendcount), (MPI_Datatype, sendtype),
                  (void*, recvbuf), (int, recvcount), (MPI_Datatype, recvtype), (int, root),
                  (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Iscatterv, (const void*, sendbuf), (const int*, sendcounts),
                    (const int*, displs), (MPI_Datatype, sendtype), (void*, recvbuf),
                    (int, recvcount), (MPI_Datatype, recvtype), (int, root), (MPI_Comm, comm),
                    (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Scatterv, MPI_Iscatterv, (const void*, sendbuf), (const int*, sendcounts),
               (const int*, displs), (MPI_Datatype, sendtype), (void*, recvbuf),
                   (int, recvcount), (MPI_Datatype, recvtype), (int, root), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Iallgather, (const void*, sendbuf), (int, sendcount),
                     (MPI_Datatype, sendtype), (void*, recvbuf), (int, recvcount),
                     (MPI_Datatype, recvtype), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Allgather, MPI_Iallgather, (const void*, sendbuf), (int, sendcount),
                    (MPI_Datatype, sendtype), (void*, recvbuf), (int, recvcount),
                    (MPI_Datatype, recvtype), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Iallgatherv, (const void*, sendbuf), (int, sendcount),
                      (MPI_Datatype, sendtype), (void*, recvbuf), (const int*, recvcounts),
                      (const int*, displs), (MPI_Datatype, recvtype), (MPI_Comm, comm),
                      (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Allgatherv, MPI_Iallgatherv, (const void*, sendbuf), (int, sendcount),
                     (MPI_Datatype, sendtype), (void*, recvbuf), (const int*, recvcounts),
                     (const int*, displs), (MPI_Datatype, recvtype), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Ialltoall, (const void*, sendbuf), (int, sendcount),
                    (MPI_Datatype, sendtype), (void*, recvbuf), (int, recvcount),
                    (MPI_Datatype, recvtype), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Alltoall, MPI_Ialltoall, (const void*, sendbuf), (int, sendcount), (MPI_Datatype, sendtype),
                   (void*, recvbuf), (int, recvcount), (MPI_Datatype, recvtype),
                   (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Ialltoallv, (const void*, sendbuf), (const int*, sendcounts),
                     (const int*, sdispls), (MPI_Datatype, sendtype), (void*, recvbuf),
                     (const int*, recvcounts), (const int*, rdispls),
                     (MPI_Datatype, recvtype), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Alltoallv, MPI_Ialltoallv, (const void*, sendbuf), (const int*, sendcounts),
                    (const int*, sdispls), (MPI_Datatype, sendtype), (void*, recvbuf),
                    (const int*, recvcounts), (const int*, rdispls),
                    (MPI_Datatype, recvtype), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Ialltoallw, (const void*, sendbuf), (const int*, sendcounts),
                     (const int*, sdispls), (const MPI_Datatype*, sendtypes),
                     (void*, recvbuf), (const int*, recvcounts), (const int*, rdispls),
                     (const MPI_Datatype*, recvtypes), (MPI_Comm, comm),
                     (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Alltoallw, MPI_Ialltoallw, (const void*, sendbuf), (const int*, sendcounts),
                    (const int*, sdispls), (const MPI_Datatype*, sendtypes),
                    (void*, recvbuf), (const int*, recvcounts), (const int*, rdispls),
                    (const MPI_Datatype*, recvtypes), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Ireduce, (const void*, sendbuf), (void*, recvbuf), (int, count),
                  (MPI_Datatype, datatype), (MPI_Op, op), (int, root), (MPI_Comm, comm),
                  (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Reduce, MPI_Ireduce, (const void*, sendbuf), (void*, recvbuf), (int, count),
                 (MPI_Datatype, datatype), (MPI_Op, op), (int, root), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Iallreduce, (const void*, sendbuf), (void*, recvbuf), (int, count),
                     (MPI_Datatype, datatype), (MPI_Op, op), (MPI_Comm, comm),
                     (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Allreduce, MPI_Iallreduce, (const void*, sendbuf), (void*, recvbuf), (int, count),
                     (MPI_Datatype, datatype), (MPI_Op, op), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Ireduce_scatter_block, (const void*, sendbuf), (void*, recvbuf),
                                (int, recvcount), (MPI_Datatype, datatype), (MPI_Op, op),
                                (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Reduce_scatter_block, MPI_Ireduce_scatter_block, (const void*, sendbuf), (void*, recvbuf),
                               (int, recvcount), (MPI_Datatype, datatype), (MPI_Op, op)
                           , (MPI_Comm,  comm))

  WRAP_NONBLOCKING(MPI_Ireduce_scatter, (const void*, sendbuf), (void*, recvbuf),
                          (const int*, recvcounts), (MPI_Datatype, datatype), (MPI_Op, op),
                          (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Reduce_scatter, MPI_Ireduce_scatter, (const void*, sendbuf), (void*, recvbuf),
                         (const int*, recvcounts), (MPI_Datatype, datatype), (MPI_Op, op),
                         (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Iscan, (const void*, sendbuf), (void*, recvbuf), (int, count),
            (MPI_Datatype, datatype), (MPI_Op, op), (MPI_Comm, comm),
            (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Scan, MPI_Iscan, (const void*, sendbuf), (void*, recvbuf), (int, count),
               (MPI_Datatype, datatype), (MPI_Op, op), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Iexscan, (const void*, sendbuf), (void*, recvbuf), (int, count),
                  (MPI_Datatype, datatype), (MPI_Op, op), (MPI_Comm, comm),
                  (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Exscan, MPI_Iexscan, (const void*, sendbuf), (void*, recvbuf), (int, count),
                 (MPI_Datatype, datatype), (MPI_Op, op), (MPI_Comm, comm))

  // Communicator management
  // int MPI_Comm_idup(MPI_Comm comm, MPI_Comm* newcomm, MPI_Request* req){
  //   // is newcomm known after nonblcoking call?
  // }

  WRAP_NEWCOMM(MPI_Comm_dup, (MPI_Comm, comm), (MPI_Comm*, newcomm))

  WRAP_NEWCOMM(MPI_Comm_dup_with_info, (MPI_Comm, comm), (MPI_Info, info), (MPI_Comm*, newcomm))
  WRAP_NEWCOMM(MPI_Comm_create, (MPI_Comm, comm), (MPI_Group, group), (MPI_Comm*, newcomm))
  WRAP_NEWCOMM(MPI_Comm_create_group, (MPI_Comm, comm), (MPI_Group, group), (int, tag),
               (MPI_Comm*, newcomm))
  WRAP_NEWCOMM(MPI_Comm_split, (MPI_Comm, comm), (int, color), (int, key), (MPI_Comm*, newcomm))
  WRAP_NEWCOMM(MPI_Comm_split_type, (MPI_Comm, comm), (int, split_type), (int, key),
               (MPI_Info, info), (MPI_Comm*, newcomm))

  int MPI_Comm_free(MPI_Comm* comm) {
    //if the communicator is revoked cancel all requests associated with it
    Comm_data* c_data = get_Comm_data(*comm);
    int result = MPI_SUCCESS;
    // cancel only if there are no pending requests,
    if(c_data->requests.size() == 0){
      result = PMPI_Comm_free(comm);
    }else{
      *comm = MPI_COMM_NULL;
    }
    // delete all associated data
    for(const MPI_Request& r : c_data->requests){
      request_comm_data.erase(r);
    }
    comm_comm_data.erase(c_data->comm);
    comm_data.erase(c_data->id);
    return result;
  }

  WRAP_NEWCOMM(MPI_Intercomm_create, (MPI_Comm, local_comm), (int, local_leader),
                           (MPI_Comm, peer_comm), (int, remote_leader), (int, tag),
                           (MPI_Comm*, newcomm))
  WRAP_NEWCOMM(MPI_Intercomm_merge, (MPI_Comm, intercomm), (int, high),
                          (MPI_Comm*, newcomm))
  // Topology communicators
  WRAP_NEWCOMM(MPI_Cart_create, (MPI_Comm, comm_old), (int, ndims), (const int*, dims),
                      (const int*, periods), (int, reorder), (MPI_Comm*, newcomm))
  WRAP_NEWCOMM(MPI_Graph_create, (MPI_Comm, comm_old), (int, nnodes), (const int*, index),
                       (const int*, edges), (int, reorder), (MPI_Comm*, newcomm))
  WRAP_NEWCOMM(MPI_Dist_graph_create_adjacent, (MPI_Comm, comm_old), (int, indegree),
               (const int*, sources), (const int*, sourceweights), (int, outdegree),
               (const int*, destinations), (const int*, destweights),
               (MPI_Info, info), (int, reorder), (MPI_Comm*, newcomm))
  WRAP_NEWCOMM(MPI_Dist_graph_create, (MPI_Comm, comm_old), (int, n), (const int*, sources),
               (const int*, degrees), (const int*, destinations),
               (const int*, weights), (MPI_Info, info), (int, reorder),
               (MPI_Comm*, newcomm))
  // Neighbor collectives
  WRAP_NONBLOCKING(MPI_Ineighbor_allgather, (const void*, sendbuf), (int, sendcount),
                              (MPI_Datatype, sendtype), (void*, recvbuf), (int, recvcount),
                              (MPI_Datatype, recvtype), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Neighbor_allgather, MPI_Ineighbor_allgather, (const void*, sendbuf), (int, sendcount),
                             (MPI_Datatype, sendtype), (void*, recvbuf), (int, recvcount),
                             (MPI_Datatype, recvtype), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Ineighbor_allgatherv, (const void*, sendbuf), (int, sendcount),
                               (MPI_Datatype, sendtype), (void*, recvbuf), (const int*, recvcounts),
                               (const int*, displs), (MPI_Datatype, recvtype), (MPI_Comm, comm),
                               (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Neighbor_allgatherv, MPI_Ineighbor_allgatherv, (const void*, sendbuf), (int, sendcount),
                              (MPI_Datatype, sendtype), (void*, recvbuf), (const int*, recvcounts),
                              (const int*, displs), (MPI_Datatype, recvtype), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Ineighbor_alltoall, (const void*, sendbuf), (int, sendcount),
                             (MPI_Datatype, sendtype), (void*, recvbuf), (int, recvcount),
                             (MPI_Datatype, recvtype), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Neighbor_alltoall, MPI_Ineighbor_alltoall, (const void*, sendbuf), (int, sendcount),
                            (MPI_Datatype, sendtype), (void*, recvbuf), (int, recvcount),
                            (MPI_Datatype, recvtype), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Ineighbor_alltoallv, (const void*, sendbuf), (const int*, sendcounts),
                              (const int*, sdispls), (MPI_Datatype, sendtype), (void*, recvbuf),
                              (const int*, recvcounts), (const int*, rdispls),
                              (MPI_Datatype, recvtype), (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Neighbor_alltoallv, MPI_Ineighbor_alltoallv, (const void*, sendbuf), (const int*, sendcounts),
                             (const int*, sdispls), (MPI_Datatype, sendtype), (void*, recvbuf),
                             (const int*, recvcounts), (const int*, rdispls),
                             (MPI_Datatype, recvtype), (MPI_Comm, comm))

  WRAP_NONBLOCKING(MPI_Ineighbor_alltoallw, (const void*, sendbuf), (const int*, sendcounts),
                              (const MPI_Aint*, sdispls), (const MPI_Datatype*, sendtypes),
                              (void*, recvbuf), (const int*, recvcounts),
                              (const MPI_Aint*, rdispls), (const MPI_Datatype*, recvtypes),
                              (MPI_Comm, comm), (MPI_Request*, req))
  WRAP_BLOCKING(MPI_Neighbor_alltoallw, MPI_Ineighbor_alltoallw, (const void*, sendbuf), (const int*, sendcounts),
                             (const MPI_Aint*, sdispls), (const MPI_Datatype*, sendtypes),
                             (void*, recvbuf), (const int*, recvcounts),
                             (const MPI_Aint*, rdispls), (const MPI_Datatype*, recvtypes),
                             (MPI_Comm, comm))

  // One-sided communication
  // TODO

  // I/O
  // TODO

  // Nonblocking completition functions
  int MPI_Waitany(int count, MPI_Request array_of_requests[], int* index,
                  MPI_Status* status){
    // check whether all complete OR INACTIVE
    int flag = 0;
    int result = MPI_Testany(count, array_of_requests, index, &flag, status);
    if(flag){
      return result;
    }
    std::vector<MPI_Request> reqs(count+1);
    //check whether one request is revoked and copy to tmp vector
    for(int i = 0; i < count; i++){
      if(array_of_requests[i] != MPI_REQUEST_NULL){
        Comm_data* cd = request_comm_data[array_of_requests[i]];
        if(cd->revoked){
          *index = i;
          array_of_requests[i] = MPI_REQUEST_NULL;
          if(status != MPI_STATUS_IGNORE)
            status->MPI_ERROR = MPIX_ERR_REVOKED;
          PMPI_Comm_call_errhandler(cd->comm, MPIX_ERR_REVOKED);
          return MPIX_ERR_REVOKED;
        }
      }
      reqs[i] = array_of_requests[i];
    }

    // the last request is the blackchannel
    reqs[count] = blackchannel;

    result = PMPI_Waitany(count+1, reqs.data(), index, status);
    // copy requests back
    for(int i = 0; i < count; i++){
      if(reqs[i] == MPI_REQUEST_NULL && array_of_requests[i] != MPI_REQUEST_NULL){
        request_comm_data[array_of_requests[i]]->requests.erase(array_of_requests[i]);
        request_comm_data.erase(array_of_requests[i]);
      }
      array_of_requests[i] = reqs[i];
    }
    blackchannel = reqs[count];

    // if the blackchannel was completed
    if(result == MPI_SUCCESS && *index == count){
      comm_data[blackchannel_data].revoked = true;
      create_blackchannel();
      // recursive call (other communicator maybe revoked)
      return MPI_Waitany(count, array_of_requests, index, status);
    }
    return result;
  }

  int MPI_Testany(int count, MPI_Request array_of_requests[], int* index,
                  int* flag, MPI_Status* status){
    *flag = 0;
    int result = PMPI_Testany(count, array_of_requests, index, flag, status);
    if(*flag)
      return result;
    result = checkBlackchannel();
    if(result != MPI_SUCCESS){
      return result;
    }

    // check whether a request was revoked
    for(int i = 0; i < count; i++){
      if(array_of_requests[i] != MPI_REQUEST_NULL){
        Comm_data* c_data = request_comm_data[array_of_requests[i]];
        if(c_data->revoked){
          *index = i;
          array_of_requests[i] = MPI_REQUEST_NULL;
          if(status != MPI_STATUS_IGNORE)
            status->MPI_ERROR = MPIX_ERR_REVOKED;
          *flag = 1;
          PMPI_Comm_call_errhandler(c_data->comm, MPIX_ERR_REVOKED);
          return MPIX_ERR_REVOKED;
        }
      }
    }

    // copy requests
    std::vector<MPI_Request> tmp(count);
    for(int i = 0; i < count; i++)
      tmp[i] = array_of_requests[i];

    result = PMPI_Testany(count, array_of_requests, index, flag, status);
    // remove request in comm_data and request_comm map
    if(*flag && *index != MPI_UNDEFINED && array_of_requests[*index] == MPI_REQUEST_NULL && tmp[*index] != MPI_REQUEST_NULL){
      request_comm_data[tmp[*index]]->requests.erase(tmp[*index]);
      request_comm_data.erase(tmp[*index]);
    }
    return result;
  }

  int MPI_Waitall(int count, MPI_Request array_of_requests[],
                  MPI_Status array_of_statuses[]){
    MPI_Status s;
    int index = -1;
    int result = MPI_SUCCESS;
    for(int i = 0; i < count; i++){
      int r = MPI_Waitany(count, array_of_requests, &index, &s);
      if(r == MPI_SUCCESS && array_of_statuses != MPI_STATUSES_IGNORE)
        array_of_statuses[index] = s;
      if(r != MPI_SUCCESS){
        result = MPI_ERR_IN_STATUS;
        if(array_of_statuses != MPI_STATUSES_IGNORE)
          array_of_statuses[index].MPI_ERROR = r;
      }
    }
    return result;
  }

  int MPI_Testall(int count, MPI_Request array_of_requests[], int* flag,
                  MPI_Status array_of_statuses[]){
    int result = checkBlackchannel();
    if(result != MPI_SUCCESS){
      return result;
    }

    // check whether a request was revoked
    int no_revoked = 0;
    result = MPI_SUCCESS;
    for(int i = 0; i < count; i++){
      if(array_of_requests[i] != MPI_REQUEST_NULL){
        Comm_data* data = request_comm_data[array_of_requests[i]];
        if(data->revoked){
          array_of_requests[i] = MPI_REQUEST_NULL;
          array_of_statuses[i].MPI_ERROR = MPIX_ERR_REVOKED;
          no_revoked++;
          PMPI_Comm_call_errhandler(data->comm, MPIX_ERR_REVOKED);
          result = MPI_ERR_IN_STATUS;
        }
      }else{
        no_revoked++;
      }
    }

    if(no_revoked == count){
      *flag = 1;
      return result;
    }

    // copy requests
    std::vector<MPI_Request> tmp(count);
    for(int i = 0; i < count; i++)
      tmp[i] = array_of_requests[i];

    result = PMPI_Testall(count, array_of_requests, flag, array_of_statuses);
    // remove request in comm_data and request_comm map
    if(*flag){
      for(int i = 0; i < count; i++){
        Comm_data* c_data = request_comm_data[tmp[i]];
        if(c_data->revoked && array_of_statuses != MPI_STATUSES_IGNORE){
          array_of_statuses[i].MPI_ERROR = MPIX_ERR_REVOKED;
          PMPI_Comm_call_errhandler(c_data->comm, MPIX_ERR_REVOKED);
          result = MPI_ERR_IN_STATUS;
        }
        if(array_of_requests[i] == MPI_REQUEST_NULL && tmp[i] != MPI_REQUEST_NULL){
          c_data->requests.erase(tmp[i]);
          request_comm_data.erase(tmp[i]);
        }
      }
    }
    return result;
  }

  int MPI_Waitsome(int incount, MPI_Request array_of_requests[],
                   int* outcount, int array_of_indices[],
                   MPI_Status array_of_statuses[]){
    // TODO: implement Waitsome
    *outcount = 1;
    int result = MPI_Waitany(incount, array_of_requests, &array_of_indices[0], &array_of_statuses[0]);
    if(result != MPI_SUCCESS){
      array_of_statuses[0].MPI_ERROR = result;
      return MPI_ERR_IN_STATUS;
    }
    return MPI_SUCCESS;
  }

  int MPI_Testsome(int incount, MPI_Request array_of_requests[],
                   int* outcount, int array_of_indices[],
                   MPI_Status array_of_statuses[]){
    // TODO: implement Testsome
    int result =  MPI_Testany(incount, array_of_requests, &array_of_indices[0], outcount, &array_of_statuses[0]);
    if(result != MPI_SUCCESS){
      array_of_statuses[*outcount].MPI_ERROR = result;
      return MPI_ERR_IN_STATUS;
    }
    return MPI_SUCCESS;
  }

  int MPI_Wait(MPI_Request* request, MPI_Status* status){
    int index;
    if(*request == MPI_REQUEST_NULL)
      return PMPI_Wait(request, status);
    return MPI_Waitany(1, request, &index, status);
  }

  int MPI_Test(MPI_Request* request, int* flag, MPI_Status* status){
    int index = 0;
    int result =  MPI_Testany(1, request, &index, flag, status);
    return result;
  }

  // ULFM functions
  int MPIX_Comm_revoke(MPI_Comm comm){
    Comm_data* c_data = get_Comm_data(comm);
    if(c_data->revoked)
      return MPI_SUCCESS;
    c_data->revoked = true;

    // send message to all ranks in comm
    MPI_Group comm_grp;
    int result = PMPI_Comm_group(comm, &comm_grp);
    if(result != MPI_SUCCESS)
      return result;
    MPI_Group bc_grp;
    result = PMPI_Comm_group(blackchannel_comm, &bc_grp);
    if(result != MPI_SUCCESS)
      return result;
    int size, rank;
    result = PMPI_Group_size(comm_grp, &size);
    if(result != MPI_SUCCESS)
      return result;
    result = PMPI_Group_rank(comm_grp, &rank);
    if(result != MPI_SUCCESS)
      return result;
    std::vector<int> comm_ranks;
    comm_ranks.reserve(size-1);
    for(int i = 0; i < size; i++)
      if(i != rank)
        comm_ranks.push_back(i);
    std::vector<int> bc_ranks(size-1);
    result = PMPI_Group_translate_ranks(comm_grp, size-1, comm_ranks.data(), bc_grp, bc_ranks.data());
    if(result != MPI_SUCCESS)
      return result;

    for(int i : bc_ranks){
      int r = PMPI_Send(&c_data->id, 1, MPI_INT, i, TAG, blackchannel_comm);
      if( r != MPI_SUCCESS && result == MPI_SUCCESS)
        result = r;
    }
    return result;
  }

  int MPIX_Comm_shrink(MPI_Comm comm, MPI_Comm* newcomm){
    int result;
    Comm_data* c_data = get_Comm_data(comm);
    MPI_Group comm_grp;
    result = PMPI_Comm_group(comm, &comm_grp);
    if(result != MPI_SUCCESS)
      return result;
    c_data->revoked = false;
    result = MPI_Comm_create(blackchannel_comm, comm_grp, newcomm);
    MPI_Errhandler eh;
    result = PMPI_Comm_get_errhandler(comm, &eh);
    if(result != MPI_SUCCESS)
      return result;
    result = PMPI_Comm_set_errhandler(*newcomm, eh);
    if(result != MPI_SUCCESS)
      return result;
    c_data->revoked = true;
    return result;
  }

  int MPIX_Comm_agree(MPI_Comm comm, int* flag){
    return PMPI_Allreduce(MPI_IN_PLACE, flag, 1, MPI_INT, MPI_BAND, comm);
  }

  int MPIX_Comm_iagree(MPI_Comm comm, int* flag, MPI_Request* req){
    int result = PMPI_Iallreduce(MPI_IN_PLACE, flag, 1, MPI_INT, MPI_BAND, comm, req);
    if(result == MPI_SUCCESS)
      request_comm_data[*req] = &comm_data[-1];
    return result;
  }

  int MPI_Error_class(int errorcode, int* errorclass){
    if(errorcode == MPIX_ERR_REVOKED){
      *errorclass = MPIX_ERR_REVOKED;
      return MPI_SUCCESS;
    }
    return PMPI_Error_class(errorcode, errorclass);
  }

  int MPI_Error_string(int errorcode, char *string, int *resultlen){
    if(errorcode == MPIX_ERR_REVOKED){
      std::string msg("blackchannel-ulfm: Communicator is revoked");
      strcpy(string, msg.data());
      *resultlen = msg.size();
      return MPI_SUCCESS;
    }
    return PMPI_Error_string(errorcode, string, resultlen);
  }
}
