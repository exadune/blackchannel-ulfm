#ifndef BLACKCHANNEL_ULFM_H
#define BLACKCHANNEL_ULFM_H

extern "C"{
  int MPIX_Comm_revoke(MPI_Comm comm);

  int MPIX_Comm_shrink(MPI_Comm comm, MPI_Comm* newcomm);

  int MPIX_Comm_agree(MPI_Comm comm, int* flag);

  int MPIX_Comm_iagree(MPI_Comm comm, int* flag, MPI_Request* req);
}

#ifndef MPIX_ERR_REVOKED
#define MPIX_ERR_REVOKED 666
#endif

#endif
