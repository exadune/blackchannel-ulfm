#include <iostream>
#include <exception>
#include <mpi.h>
#include "../blackchannel-ulfm.h"

int rank = -1;

MPI_Comm getWorldDup(){
  MPI_Comm world2;
  MPI_Comm_dup(MPI_COMM_WORLD, &world2);
  return world2;
}

void testTest(){
  std::cout  << "rank " << rank << ": --- testTest:" << std::endl;
  MPI_Comm comm = getWorldDup();
  MPI_Request req;
  MPI_Ibarrier(comm, &req);
  int flag = 0, result = MPI_SUCCESS;
  while(!flag && result == MPI_SUCCESS){
    result = MPI_Test(&req, &flag, MPI_STATUS_IGNORE);
  }
  if(result == MPI_SUCCESS)
    std::cout << "rank " << rank << ": Barrier successful" << std::endl;
  else
    throw std::runtime_error("Barrier failed!");

  MPI_Barrier(comm);
  result = MPIX_Comm_revoke(comm);
  if(result == MPI_SUCCESS)
    std::cout  << "rank " << rank << ": Revoke successful" << std::endl;
  else
    throw std::runtime_error("Revoke failed!");

  MPI_Ibarrier(comm, &req);
  if(req == MPI_REQUEST_NULL)
    return;
  flag = 0;
  while(!flag){
    result = MPI_Test(&req, &flag, MPI_STATUS_IGNORE);
  }
  if(result == MPIX_ERR_REVOKED)
    std::cout  << "rank " << rank << ": Barrier successfully revoked!" << std::endl;
  else
    throw std::runtime_error("Revoked Barrier failed!");
}

void testWait(){
  std::cout  << "rank " << rank << ": --- testWait:" << std::endl;
  MPI_Comm comm = getWorldDup();
  MPI_Request req;
  int result;
  MPI_Ibarrier(comm, &req);
  result = MPI_SUCCESS;
  result = MPI_Wait(&req, MPI_STATUS_IGNORE);
  if(result == MPI_SUCCESS)
    std::cout  << "rank " << rank << ": Barrier successful" << std::endl;
  else
    throw std::runtime_error("Barrier failed!");

  MPI_Barrier(comm);
  result = MPIX_Comm_revoke(comm);
  if(result == MPI_SUCCESS)
    std::cout  << "rank " << rank << ": Revoke successful" << std::endl;
  else
    throw std::runtime_error("Revoke failed!");

  MPI_Ibarrier(comm, &req);
  if(req == MPI_REQUEST_NULL)
    return;
  result = MPI_Wait(&req, MPI_STATUS_IGNORE);

  if(result == MPIX_ERR_REVOKED)
    std::cout  << "rank " << rank << ": Barrier successfully revoked!" << std::endl;
  else
    throw std::runtime_error("Revoked Barrier failed!");
}


void testTestall(){
  std::cout  << "rank " << rank << ": --- testTestall:" << std::endl;
  MPI_Comm comm = getWorldDup();
  MPI_Request req[2];
  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  int flag = 0, result = MPI_SUCCESS;
  while(!flag && result == MPI_SUCCESS){
    result = MPI_Testall(2, req, &flag, MPI_STATUSES_IGNORE);
  }
  if(result == MPI_SUCCESS)
    std::cout << "rank " << rank << ": Barrier successful" << std::endl;
  else
    throw std::runtime_error("Barrier failed!");

  MPI_Barrier(comm);
  result = MPIX_Comm_revoke(comm);
  if(result == MPI_SUCCESS)
    std::cout  << "rank " << rank << ": Revoke successful" << std::endl;
  else
    throw std::runtime_error("Revoke failed!");

  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  if(req[0] == MPI_REQUEST_NULL && req[1] == MPI_REQUEST_NULL)
    return;
  flag = 0;
  MPI_Status s[2];
  while(!flag && result == MPI_SUCCESS){
    result = MPI_Testall(2, req, &flag, s);
  }
  if(result == MPI_ERR_IN_STATUS && s[0].MPI_ERROR == MPIX_ERR_REVOKED && s[1].MPI_ERROR == MPIX_ERR_REVOKED)
    std::cout  << "rank " << rank << ": Barrier successfully revoked!" << std::endl;
  else
    throw std::runtime_error("Revoked Barrier failed!");
}

void testWaitall(){
  std::cout  << "rank " << rank << ": --- testWaitall:" << std::endl;
  MPI_Comm comm = getWorldDup();
  MPI_Request req[2];
  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  int flag = 0, result = MPI_SUCCESS;
  result = MPI_Waitall(2, req, MPI_STATUSES_IGNORE);
  if(result == MPI_SUCCESS)
    std::cout << "rank " << rank << ": Barrier successful" << std::endl;
  else
    throw std::runtime_error("Barrier failed!");

  MPI_Barrier(comm);
  result = MPIX_Comm_revoke(comm);
  if(result == MPI_SUCCESS)
    std::cout  << "rank " << rank << ": Revoke successful" << std::endl;
  else
    throw std::runtime_error("Revoke failed!");

  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  if(req[0] == MPI_REQUEST_NULL && req[1] == MPI_REQUEST_NULL)
    return;
  flag = 0;
  MPI_Status s[2];
  result = MPI_Waitall(2, req, s);
  if(result == MPI_ERR_IN_STATUS && (s[0].MPI_ERROR == MPIX_ERR_REVOKED && s[1].MPI_ERROR == MPIX_ERR_REVOKED))
    std::cout  << "rank " << rank << ": Barrier successfully revoked!" << std::endl;
  else
    throw std::runtime_error("Revoked Barrier failed!");
}

void testTestany(){
  std::cout  << "rank " << rank << ": --- testTestany:" << std::endl;
  MPI_Comm comm = getWorldDup();
  MPI_Request req[2];
  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  int flag = 0, result = MPI_SUCCESS, index = 0;
  while(!flag && result == MPI_SUCCESS){
    result = MPI_Testany(2, req, &index, &flag, MPI_STATUS_IGNORE);
  }
  if(result == MPI_SUCCESS)
    std::cout << "rank " << rank << ": Barrier successful" << std::endl;
  else
    throw std::runtime_error("Barrier failed!");

  MPI_Barrier(comm);
  result = MPIX_Comm_revoke(comm);
  if(result == MPI_SUCCESS)
    std::cout  << "rank " << rank << ": Revoke successful" << std::endl;
  else
    throw std::runtime_error("Revoke failed!");

  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  if(req[0] == MPI_REQUEST_NULL && req[1] == MPI_REQUEST_NULL)
    return;
  flag = 0;
  while(!flag && result == MPI_SUCCESS){
    result = MPI_Testany(2, req, &index, &flag, MPI_STATUS_IGNORE);
  }
  if(result == MPIX_ERR_REVOKED)
    std::cout  << "rank " << rank << ": Barrier successfully revoked!" << std::endl;
  else
    throw std::runtime_error("Revoked Barrier failed!");
}

void testWaitany(){
  std::cout  << "rank " << rank << ": --- testWaitany:" << std::endl;
  MPI_Comm comm = getWorldDup();
  MPI_Request req[2];
  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  int flag = 0, result = MPI_SUCCESS, index;
  result = MPI_Waitany(2, req, &index, MPI_STATUSES_IGNORE);
  if(result == MPI_SUCCESS)
    std::cout << "rank " << rank << ": Barrier successful" << std::endl;
  else
    throw std::runtime_error("Barrier failed!");

  MPI_Barrier(comm);
  result = MPIX_Comm_revoke(comm);
  if(result == MPI_SUCCESS)
    std::cout  << "rank " << rank << ": Revoke successful" << std::endl;
  else
    throw std::runtime_error("Revoke failed!");

  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  if(req[0] == MPI_REQUEST_NULL && req[1] == MPI_REQUEST_NULL)
    return;
  flag = 0;
  result = MPI_Waitany(2, req, &index, MPI_STATUSES_IGNORE);
  if(result == MPIX_ERR_REVOKED)
    std::cout  << "rank " << rank << ": Barrier successfully revoked!" << std::endl;
  else
    throw std::runtime_error("Revoked Barrier failed!");
}

void testTestsome(){
  std::cout  << "rank " << rank << ": --- testTestsome:" << std::endl;
  MPI_Comm comm = getWorldDup();
  MPI_Request req[2];
  int indices[2];
  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  int flag = 0, result = MPI_SUCCESS, index = 0;
  while(!flag && result == MPI_SUCCESS){
    result = MPI_Testsome(2, req, &flag, indices, MPI_STATUSES_IGNORE);
  }
  if(result == MPI_SUCCESS)
    std::cout << "rank " << rank << ": Barrier successful" << std::endl;
  else
    throw std::runtime_error("Barrier failed!");

  MPI_Barrier(comm);
  result = MPIX_Comm_revoke(comm);
  if(result == MPI_SUCCESS)
    std::cout  << "rank " << rank << ": Revoke successful" << std::endl;
  else
    throw std::runtime_error("Revoke failed!");

  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  if(req[0] == MPI_REQUEST_NULL && req[1] == MPI_REQUEST_NULL)
    return;
  flag = 0;
  MPI_Status s[2];
  while(!flag && result == MPI_SUCCESS){
    result = MPI_Testsome(2, req, &flag, indices, s);
  }
  if(result == MPI_ERR_IN_STATUS && s[indices[0]].MPI_ERROR == MPIX_ERR_REVOKED)
    std::cout  << "rank " << rank << ": Barrier successfully revoked!" << std::endl;
  else
    throw std::runtime_error("Revoked Barrier failed!");
}

void testWaitsome(){
    std::cout  << "rank " << rank << ": --- testWaitsome:" << std::endl;
  MPI_Comm comm = getWorldDup();
  MPI_Request req[2];
  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  int flag = 0, result = MPI_SUCCESS, index;
  int indices[2];
  result = MPI_Waitsome(2, req, &flag, indices, MPI_STATUSES_IGNORE);
  if(result == MPI_SUCCESS)
    std::cout << "rank " << rank << ": Barrier successful" << std::endl;
  else
    throw std::runtime_error("Barrier failed!");

  MPI_Barrier(comm);
  result = MPIX_Comm_revoke(comm);
  if(result == MPI_SUCCESS)
    std::cout  << "rank " << rank << ": Revoke successful" << std::endl;
  else
    throw std::runtime_error("Revoke failed!");

  MPI_Ibarrier(comm, &req[0]);
  MPI_Ibarrier(comm, &req[1]);
  if(req[0] == MPI_REQUEST_NULL && req[1] == MPI_REQUEST_NULL)
    return;
  flag = 0;
  MPI_Status s[2];
  result = MPI_Waitsome(2, req, &flag, indices, s);
  if(result == MPI_ERR_IN_STATUS && s[indices[0]].MPI_ERROR == MPIX_ERR_REVOKED)
    std::cout  << "rank " << rank << ": Barrier successfully revoked!" << std::endl;
  else
    throw std::runtime_error("Revoked Barrier failed!");
}

int main(int argc, char** argv){
  MPI_Init(&argc, &argv);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  try{
    testTest();
    testWait();
    testTestall();
    testWaitall();
    testTestany();
    testWaitany();
    testTestsome();
    testWaitsome();
  }catch(std::exception& e){
    std::cerr << "Exception on rank "<< rank << ": " << e.what() << std::endl;
    MPI_Abort(MPI_COMM_WORLD, 1);
    return 1;
  }
  MPI_Finalize();
  return 0;
}
