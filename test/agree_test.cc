#include <iostream>

#include <mpi.h>
#include "../blackchannel-ulfm.h"

int main(int argc, char** argv){
  MPI_Init(&argc, &argv);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
  int rank = -1;
  int size = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPIX_Comm_revoke(MPI_COMM_WORLD);
  int flag = rank==0;
  int result = MPIX_Comm_agree(MPI_COMM_WORLD, &flag);
  if(result != MPI_SUCCESS)
    return 1;
  if(flag != 0 && size != 1)
    return 1;

  MPI_Request req;
  flag = rank==0;
  result = MPIX_Comm_iagree(MPI_COMM_WORLD, &flag, &req);
  if(result != MPI_SUCCESS)
    return 1;
  result = MPI_Wait(&req, MPI_STATUS_IGNORE);
  if(flag != 0 && size != 1)
    return 1;
  MPI_Finalize();
  return 0;
}
