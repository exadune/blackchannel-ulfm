#include <iostream>

#include <mpi.h>
#include "../blackchannel-ulfm.h"

int main(int argc, char** argv){
  int result = MPI_Init(&argc, &argv);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
  if(result != MPI_SUCCESS)
    return 1;
  MPI_Request req;
  MPI_Status status;

  int rank = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  result = MPI_Ibarrier(MPI_COMM_WORLD, &req);
  if(result != MPI_SUCCESS)
    std::cout << "Ibarrier failed: " << result << std::endl;
  result = MPI_Wait(&req, &status);
  if(result != MPI_SUCCESS)
    std::cout << "Wait failed: " << result << std::endl;

  if(rank == 0){
    result = MPIX_Comm_revoke(MPI_COMM_WORLD);
    if(result != MPI_SUCCESS)
      std::cout << "revoke failed: " << result << std::endl;
  }else{
    result = MPI_Ibarrier(MPI_COMM_WORLD, &req);
    if(result != MPI_SUCCESS)
      std::cout << "Ibarrier failed: " << result << std::endl;
    result = MPI_Wait(&req, &status);
    if(result != MPI_SUCCESS)
      std::cout << "Wait failed: " << result << std::endl;
  }
  MPI_Finalize();
  return 0;
}
