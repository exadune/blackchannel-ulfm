#include <iostream>
#include <mpi.h>

#include "../blackchannel-ulfm.h"

int main(int argc, char** argv){
  MPI_Init(&argc, &argv);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
  MPI_Comm world2;
  MPI_Comm_dup(MPI_COMM_WORLD, &world2);
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  int color = rank%3;
  MPI_Comm splitted;
  MPI_Comm_split(world2, color, rank, &splitted);
  MPI_Comm splitted2;
  MPI_Comm_dup(splitted, &splitted2);
  if(rank == 0)
    MPIX_Comm_revoke(splitted);

  int result = MPI_Barrier(splitted2);
  if(result == MPIX_ERR_REVOKED)
    return 1;

  result = MPI_Barrier(splitted);
  if(result == MPIX_ERR_REVOKED && rank%3 != 0)
    return 1;
  if(result != MPIX_ERR_REVOKED && rank%3 == 0 && size > 1)
    return 1;
  MPI_Finalize();
  return 0;
}
