#include <iostream>
#include <mpi.h>
#include "../blackchannel-ulfm.h"

class MPIError {
public:
  /** @brief Constructor. */
  MPIError(std::string s, int e) : errorstring(s), errorcode(e){}
  /** @brief The error string. */
  std::string errorstring;
  /** @brief The mpi error code. */
  int errorcode;
};

void MPI_err_handler(MPI_Comm *, int *err_code, ...){
  char *err_string=new char[MPI_MAX_ERROR_STRING];
  int err_length;
  MPI_Error_string(*err_code, err_string, &err_length);
  std::string s(err_string, err_length);
  std::cerr << "An MPI Error ocurred:"<<std::endl<<s<<std::endl;
  delete[] err_string;
  throw MPIError(s, *err_code);
}



int main(int argc, char** argv){
  MPI_Init(&argc, &argv);
  int size = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm world2;
  MPI_Comm_dup(MPI_COMM_WORLD, &world2);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
  MPI_Errhandler errhandler;
  MPI_Comm_create_errhandler(MPI_err_handler, &errhandler);
  MPI_Comm_set_errhandler(world2, errhandler);

  {
    int result = MPI_Send(nullptr, 1, MPI_INT, 0, 42, MPI_COMM_WORLD);
    if(result == MPI_SUCCESS){
      std::cerr << "Thats weird!" << std::endl;
      return result;
    }
  }

  try{
    int result = MPI_Send(nullptr, 1, MPI_INT, 0, 42, world2);
    std::cerr << "Thats weird!" << std::endl;
    return 1;
  }catch(MPIError& e){
    std::cout << "Exception: " << e.errorstring << std::endl;
  }

  MPIX_Comm_revoke(world2);
  try{
    int result = MPI_Barrier(world2);
    if(size != 1){
      std::cerr << "Thats weird!" << std::endl;
      return 1;
    }
  }catch(MPIError& e){
    std::cout << "Exception: " << e.errorstring << std::endl;
  }
  MPI_Finalize();
  return 0;
}
