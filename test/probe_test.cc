#include <iostream>

#include <mpi.h>
#include "../blackchannel-ulfm.h"

int main(int argc, char** argv){
  MPI_Init(&argc, &argv);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);

  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  int dest = (rank+1)%size;
  MPI_Request req;
  MPI_Isend(&rank, 1, MPI_INT, dest, 0, MPI_COMM_WORLD, &req);
  int result = MPI_Probe(MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if(result != MPI_SUCCESS){
    std::cerr << "First Probe failed (" << result << ")" << std::endl;
    return 1;
  }
  int source;
  result = MPI_Recv(&source, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if(result != MPI_SUCCESS)
    return 1;
  result = MPI_Wait(&req, MPI_STATUS_IGNORE);

  MPI_Barrier(MPI_COMM_WORLD);

  if(rank == 0)
    MPIX_Comm_revoke(MPI_COMM_WORLD);

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Isend(&rank, 1, MPI_INT, dest, 0, MPI_COMM_WORLD, &req);


  result = MPI_Probe(MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if(result != MPIX_ERR_REVOKED){
    std::cerr << "Probe did not returned MPIX_ERR_REVOKED" << std::endl;
    return 1;
  }

  result = MPI_Recv(&source, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  result = MPI_Wait(&req, MPI_STATUS_IGNORE);

  MPI_Finalize();
  return 0;
}
