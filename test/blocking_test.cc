#include <iostream>
#include <exception>
#include <vector>
#include <numeric>

#include <mpi.h>
#include "../blackchannel-ulfm.h"

/*
  This test calls all blocking functions on a non-revoked and revoked communicator.
  All the blocking calls are mapped to the corresponding nonblockings,
  that is the nonblocking calls are also covered by this test.
 */

MPI_Comm topo_comm;
MPI_Comm revoked_comm;
int size, rank;

template<class F>
void test(F fun){
  int result = fun(topo_comm);
  if(result != MPI_SUCCESS)
    throw new std::exception();

  result = fun(revoked_comm);
  if(result != MPIX_ERR_REVOKED && size > 1)
    throw new std::exception();
}


int main(int argc, char** argv){
  MPI_Init(&argc, &argv);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int period = 1;
  MPI_Cart_create(MPI_COMM_WORLD, 1, &size, &period, 0, &topo_comm);
  MPI_Comm_dup(topo_comm, &revoked_comm);
  MPIX_Comm_revoke(revoked_comm);

  int buf = 42;
  std::vector<int> buf_size(size, 43);
  std::vector<int> ones(size, 1);
  std::vector<int> displ(size);
  std::vector<MPI_Aint> displ2(size);
  std::iota(displ.begin(), displ.end(), 0);
  std::iota(displ2.begin(), displ2.end(), 0);
  std::vector<MPI_Datatype> datatypes(size, MPI_INT);

  // reserve buffer for MPI_Bsend
  char mpi_buf[sizeof(int)+MPI_BSEND_OVERHEAD];
  int result = MPI_Buffer_attach(&mpi_buf, sizeof(int)+MPI_BSEND_OVERHEAD);

  // POINT2POINT
  if(size > 1){
    if(rank == 0){
      test([&](MPI_Comm c){return MPI_Send(&buf, 1, MPI_INT, 1, 0, c);});
      test([&](MPI_Comm c){return MPI_Bsend(&buf, 1, MPI_INT, 1, 0, c);});
      test([&](MPI_Comm c){return MPI_Ssend(&buf, 1, MPI_INT, 1, 0, c);});
    }
    else if(rank == 1){
      test([&](MPI_Comm c){return MPI_Recv(&buf, 1, MPI_INT, 0, 0, c, MPI_STATUS_IGNORE);});
      test([&](MPI_Comm c){return MPI_Recv(&buf, 1, MPI_INT, 0, 0, c, MPI_STATUS_IGNORE);});
      test([&](MPI_Comm c){return MPI_Recv(&buf, 1, MPI_INT, 0, 0, c, MPI_STATUS_IGNORE);});
    }
  }


  // COLLECTIVE
  test([](MPI_Comm c){return MPI_Barrier(c);});
  test([&](MPI_Comm c){return MPI_Bcast(&buf, 1, MPI_INT, 0, c);});
  test([&](MPI_Comm c){return MPI_Gather(&buf, 1, MPI_INT, buf_size.data(), 1, MPI_INT, 0, c);});
  test([&](MPI_Comm c){return MPI_Gatherv(&buf, 1, MPI_INT, buf_size.data(), ones.data(),
                                          displ.data(), MPI_INT, 0, c);});
  test([&](MPI_Comm c){return MPI_Scatter(buf_size.data(), 1, MPI_INT, &buf, 1, MPI_INT, 0, c);});
  test([&](MPI_Comm c){return MPI_Scatterv(buf_size.data(), ones.data(), displ.data(), MPI_INT, &buf, 1,
                                           MPI_INT, 0, c);});
  test([&](MPI_Comm c){return MPI_Allgather(buf_size.data(), 1, MPI_INT, &buf, 1, MPI_INT, c);});
  test([&](MPI_Comm c){return MPI_Allgatherv(&buf, 1, MPI_INT, buf_size.data(), ones.data(),
                                             displ.data(), MPI_INT, c);});
  test([&](MPI_Comm c){return MPI_Alltoall(ones.data(), 1, MPI_INT, buf_size.data(), 1,
                                             MPI_INT, c);});
  test([&](MPI_Comm c){return MPI_Alltoallv(ones.data(), ones.data(), displ.data(), MPI_INT,
                                            buf_size.data(), ones.data(), displ.data(), MPI_INT, c);});
  test([&](MPI_Comm c){return MPI_Alltoallw(ones.data(), ones.data(), displ.data(), datatypes.data(),
                                            buf_size.data(), ones.data(), displ.data(), datatypes.data(), c);});
  test([&](MPI_Comm c){return MPI_Reduce(&rank, &buf, 1, MPI_INT, MPI_SUM, 0, c);});
  test([&](MPI_Comm c){return MPI_Allreduce(MPI_IN_PLACE, &buf, 1, MPI_INT, MPI_SUM, c);});
  test([&](MPI_Comm c){return MPI_Reduce_scatter_block(buf_size.data(), &buf, 1, MPI_INT, MPI_SUM, c);});
  test([&](MPI_Comm c){return MPI_Reduce_scatter(buf_size.data(), &buf, ones.data(), MPI_INT, MPI_SUM, c);});
  test([&](MPI_Comm c){return MPI_Scan(MPI_IN_PLACE, &buf, 1, MPI_INT, MPI_SUM, c);});
  test([&](MPI_Comm c){return MPI_Exscan(MPI_IN_PLACE, &buf, 1, MPI_INT, MPI_SUM, c);});
  test([&](MPI_Comm c){return MPI_Allgather(&buf, 1, MPI_INT, buf_size.data(), 1, MPI_INT, c);});
  test([&](MPI_Comm c){return MPI_Allgatherv(&buf, 1, MPI_INT, buf_size.data(), ones.data(),
                                          displ.data(), MPI_INT, c);});

  // NEIGHBORING
  if(size > 1){
    test([&](MPI_Comm c){return MPI_Neighbor_alltoall(ones.data(), 1, MPI_INT, buf_size.data(), 1,
                                                      MPI_INT, c);});
    test([&](MPI_Comm c){return MPI_Neighbor_alltoallv(ones.data(), ones.data(), displ.data(), MPI_INT,
                                                       buf_size.data(), ones.data(), displ.data(), MPI_INT, c);});
    test([&](MPI_Comm c){return MPI_Neighbor_alltoallw(ones.data(), ones.data(), displ2.data(), datatypes.data(),
                                                       buf_size.data(), ones.data(), displ2.data(), datatypes.data(), c);});
  }

  MPI_Finalize();
  return 0;
}
