#include <iostream>

#include <mpi.h>
#include "../blackchannel-ulfm.h"

int main(int argc, char** argv){
  MPI_Init(&argc, &argv);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
  int rank, size, result;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  result = MPI_Barrier(MPI_COMM_WORLD); // this barrier can fail when rank 0 is very fast!

  if(rank == 0){
    MPIX_Comm_revoke(MPI_COMM_WORLD);
    std::cout << "-- revoked" << std::endl;
  }
  else{
    result = MPI_Barrier(MPI_COMM_WORLD);
    std::cout << "-- retured from barrier with " << result << std::endl;
    if(result != MPIX_ERR_REVOKED)
      return 1;
  }
  MPI_Comm newworld;
  result = MPIX_Comm_shrink(MPI_COMM_WORLD, &newworld);
  std::cout << "result of shrink was " << result << std::endl;

  // comm world is still revoked:
  result = MPI_Barrier(MPI_COMM_WORLD);
  if(result != MPIX_ERR_REVOKED && size > 1){
    std::cout << "Barrier on revoked COMM_WORLD not returned MPIX_ERR_REVOKED" << std::endl;
    return 3;
  }

  // new works:
  result = MPI_Barrier(newworld);
  if(result != MPI_SUCCESS){
    std::cout << "Barrier on newcomm does not succeed" << std::endl;
    return 2;
  }

  MPI_Finalize();
  return 0;
}
