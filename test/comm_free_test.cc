#include <iostream>

#include <mpi.h>
#include "../blackchannel-ulfm.h"

int main(int argc, char** argv){
  MPI_Init(&argc, &argv);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
  int rank = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm comm2;
  MPI_Comm_dup(MPI_COMM_WORLD, &comm2);
  if(rank == 0)
    MPIX_Comm_revoke(comm2);
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Request req;
  int result = MPI_Ibarrier(comm2, &req);
  result = MPI_Wait(&req, MPI_STATUS_IGNORE);
  MPI_Comm_free(&comm2);
  MPI_Finalize();
  return 0;
}
